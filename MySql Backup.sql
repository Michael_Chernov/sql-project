CREATE DATABASE  IF NOT EXISTS `space_station` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `space_station`;
-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: space_station
-- ------------------------------------------------------
-- Server version	5.7.19-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `captain`
--

DROP TABLE IF EXISTS `captain`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `captain` (
  `CaptainID` int(10) NOT NULL AUTO_INCREMENT,
  `FullName` varchar(45) DEFAULT NULL,
  `OriginCountry` varchar(45) DEFAULT NULL,
  `ControlRoomID` int(11) DEFAULT NULL,
  PRIMARY KEY (`CaptainID`),
  UNIQUE KEY `CaptainID_UNIQUE` (`CaptainID`),
  KEY `Captain_Control_idx` (`ControlRoomID`),
  CONSTRAINT `Captain_Control` FOREIGN KEY (`ControlRoomID`) REFERENCES `control_room` (`ControlRoomID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `captain`
--

LOCK TABLES `captain` WRITE;
/*!40000 ALTER TABLE `captain` DISABLE KEYS */;
INSERT INTO `captain` VALUES (1,'Michael Chernov','Israel',4),(2,'Ching Chong','China',5);
/*!40000 ALTER TABLE `captain` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `control_room`
--

DROP TABLE IF EXISTS `control_room`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `control_room` (
  `ControlRoomID` int(11) NOT NULL AUTO_INCREMENT,
  `Type` varchar(45) DEFAULT NULL,
  `CurrentCrewNum` int(11) DEFAULT NULL,
  `AssignedCrewNum` int(11) DEFAULT NULL,
  `OxygenPercentage` int(11) DEFAULT NULL,
  `SpaceshipID` int(11) DEFAULT NULL,
  PRIMARY KEY (`ControlRoomID`),
  UNIQUE KEY `Control room ID_UNIQUE` (`ControlRoomID`),
  KEY `Space_Control_idx` (`SpaceshipID`),
  CONSTRAINT `Space_Control` FOREIGN KEY (`SpaceshipID`) REFERENCES `spaceship` (`SpaceshipID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `control_room`
--

LOCK TABLES `control_room` WRITE;
/*!40000 ALTER TABLE `control_room` DISABLE KEYS */;
INSERT INTO `control_room` VALUES (1,'Engine',1,2,89,1),(2,'Oxygen',4,1,43,1),(3,'Weapons',0,4,85,1),(4,'Cockpit',1,1,79,1),(5,'Cockpit',1,1,98,2),(6,'Oxygen',1,1,100,2),(7,'Engine',2,2,100,2);
/*!40000 ALTER TABLE `control_room` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `crew_member`
--

DROP TABLE IF EXISTS `crew_member`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `crew_member` (
  `CrewMemberID` int(11) NOT NULL AUTO_INCREMENT,
  `FullName` varchar(45) NOT NULL,
  `OriginCountry` varchar(45) DEFAULT NULL,
  `CurrentRoom` int(11) DEFAULT NULL,
  `CaptainID` int(11) DEFAULT NULL,
  `ControlRoomID` int(11) DEFAULT NULL,
  PRIMARY KEY (`CrewMemberID`),
  KEY `Crew_Control_idx` (`ControlRoomID`),
  KEY `Crew_Captain_idx` (`CaptainID`),
  CONSTRAINT `Crew_Captain` FOREIGN KEY (`CaptainID`) REFERENCES `captain` (`CaptainID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `Crew_Control` FOREIGN KEY (`ControlRoomID`) REFERENCES `control_room` (`ControlRoomID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `crew_member`
--

LOCK TABLES `crew_member` WRITE;
/*!40000 ALTER TABLE `crew_member` DISABLE KEYS */;
INSERT INTO `crew_member` VALUES (1,'Shmulik Kool','Israel',2,1,2),(2,'Jack Mcdean','U.S',1,1,1),(3,'Erik Geroge','U.S',2,1,1),(4,'Max Damon','U.S',2,1,3),(5,'Merlin Frank','Britain',2,1,3),(6,'Ching Chang','China',7,2,7),(7,'Vlad Blyatyob','Russia',7,2,7),(8,'Chin Chin Chong','China',6,2,6);
/*!40000 ALTER TABLE `crew_member` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `device`
--

DROP TABLE IF EXISTS `device`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `device` (
  `DeviceID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(45) DEFAULT NULL,
  `State` varchar(45) DEFAULT NULL,
  `ControlRoomID` int(11) DEFAULT NULL,
  PRIMARY KEY (`DeviceID`),
  KEY `Control room ID_idx` (`ControlRoomID`),
  CONSTRAINT `Device_Control` FOREIGN KEY (`ControlRoomID`) REFERENCES `control_room` (`ControlRoomID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `device`
--

LOCK TABLES `device` WRITE;
/*!40000 ALTER TABLE `device` DISABLE KEYS */;
INSERT INTO `device` VALUES (1,'Engine 1','Working',1),(2,'Engine 2','Working',1),(3,'Oxygen Genrator','Broken',2),(4,'Laser Blaster Mk 2','Idle',3),(5,'Engine 1','Idle',7),(6,'Engine 2','Idle',7),(7,'Engine 3','Idle',7),(8,'Oxygen Generator','Working',6);
/*!40000 ALTER TABLE `device` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `spaceship`
--

DROP TABLE IF EXISTS `spaceship`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `spaceship` (
  `SpaceshipID` int(11) NOT NULL AUTO_INCREMENT,
  `Country` varchar(45) DEFAULT NULL,
  `State` varchar(45) DEFAULT NULL,
  `Type` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`SpaceshipID`),
  UNIQUE KEY `Spaceship ID_UNIQUE` (`SpaceshipID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `spaceship`
--

LOCK TABLES `spaceship` WRITE;
/*!40000 ALTER TABLE `spaceship` DISABLE KEYS */;
INSERT INTO `spaceship` VALUES (1,'Israel','Flying','Military'),(2,'China','Parked','Merchant');
/*!40000 ALTER TABLE `spaceship` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-10-25 10:56:30
